## Console Chess
To make move use checkerboard coordinates \
For example move `A2 A3` means moving a figure \
from `A2` to `A3`

Other command: \
`EXIT` - close program \
`SAVE` - save current game progress \
`LOAD` - load saved game if you have file save.txt