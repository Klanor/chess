#ifndef CHESS_PARSER_H
#define CHESS_PARSER_H

#include <string>
#include <exception>
#include <vector>

class Parser {
    const static std::vector<int> cof;

    static size_t parseY(const char *val);

    static size_t parseX(char val);

public:
    static std::pair<std::pair<size_t, size_t>, std::pair<size_t, size_t>> Parse(const std::string &str);
};

#endif //CHESS_PARSER_H
