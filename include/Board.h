#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include "../src/include/Piece.h"

class Board {
    char player = 'W';
    bool checkmate = false;
    std::vector<std::vector<Piece *>> *chess_board;
    std::vector<std::vector<Piece *>> *check_chess_board;

public:
    Board();

    void play(std::pair<std::pair<size_t, size_t>, std::pair<size_t, size_t>> val);

    void printBoard();

    void updateBoard(std::vector<std::vector<Piece *>> &board);

    void save(const char *path);

    void load(const char *path);

    bool checkField(size_t y, size_t x);

    bool castling(size_t xR, size_t y, size_t xK);

    bool checkMate(Piece &figure);

    bool is_checkMate() const;

    std::vector<std::vector<Piece *>> &getVectorChess() const;

    char currentPlayer() const;

    ~Board();
};

#endif //CHESS_BOARD_H
