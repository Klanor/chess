cmake_minimum_required(VERSION 3.16)
project(Chess)

set(CMAKE_CXX_STANDARD 17)

add_executable(Chess src/Chess.cpp src/Board.cpp src/Piece.cpp src/Parser.cpp)

add_subdirectory(googletest)
include_directories(${gtest_SOURSE_DIR}/include ${gtest_SOURCE_DIR})
add_executable(Gtest src/gTest/gTestSuite.cpp src/Board.cpp src/Piece.cpp src/Parser.cpp)
target_link_libraries(Gtest gtest gtest_main)