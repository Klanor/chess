#include "../../include/Board.h"
#include "../../include/Parser.h"
#include <gtest/gtest.h>

TEST(Chess, input) {
    ASSERT_ANY_THROW(Parser::Parse("dfjoioijd"));
    ASSERT_ANY_THROW(Parser::Parse("a3 a7"));
    ASSERT_ANY_THROW(Parser::Parse("A2 A9"));
    ASSERT_ANY_THROW(Parser::Parse("A2 Z9"));
    ASSERT_ANY_THROW(Parser::Parse(" "));
    ASSERT_ANY_THROW(Parser::Parse("sa lk"));
    ASSERT_ANY_THROW(Parser::Parse("D2 A"));
    ASSERT_NO_THROW(Parser::Parse("A7 A5"));
}

TEST(Chess, output) {
    auto t = Parser::Parse("A4 B5");
    EXPECT_EQ(t.first.first, 4);
    EXPECT_EQ(t.first.second, 0);
    EXPECT_EQ(t.second.first, 3);
    EXPECT_EQ(t.second.second, 1);
}

TEST(Chess, Turn_test) {
    Board brd;

    auto white_turn1 = Parser::Parse("A2 A3");
    ASSERT_NO_THROW(brd.play(white_turn1));

    auto white_turn2 = Parser::Parse("A3 A4");
    ASSERT_ANY_THROW(brd.play(white_turn2));

    auto black_turn1 = Parser::Parse("A7 A6");
    ASSERT_NO_THROW(brd.play(black_turn1));

    ASSERT_NO_THROW(brd.play(white_turn2));
}

TEST(Chess, save_load) {
    Board *brd;
    auto turn = Parser::Parse("A2 A3");

    brd = new Board;
    brd->play(turn);
    std::string name_figure = brd->getVectorChess().at(turn.second.first).at(turn.second.second)->getName();

    brd->save("test_save.txt");
    delete brd;

    brd = new Board;
    ASSERT_ANY_THROW(brd->load("nonexistent_file.txt"));
    ASSERT_NO_THROW(brd->load("test_save.txt"));

    std::string name_figure_after = brd->getVectorChess().at(turn.second.first).at(turn.second.second)->getName();
    ASSERT_EQ(name_figure, name_figure_after);
}

TEST(Chess, Pawn) {
    Board brd;

    auto white_long_turn = Parser::Parse("A2 A4");
    ASSERT_NO_THROW(brd.play(white_long_turn));

    auto black_short_turn = Parser::Parse("B7 B6");
    ASSERT_NO_THROW(brd.play(black_short_turn));

    auto white_move_back = Parser::Parse("A4 A3");
    ASSERT_ANY_THROW(brd.play(white_move_back));

    auto white_short_turn = Parser::Parse("A4 A5");
    ASSERT_NO_THROW(brd.play(white_short_turn));

    auto black_long_turn = Parser::Parse("B6 B4");
    ASSERT_ANY_THROW(brd.play(black_long_turn));

    auto black_uncorect_take = Parser::Parse("A7 A5");
    ASSERT_ANY_THROW(brd.play(black_uncorect_take));

    auto black_correct_take = Parser::Parse("B6 A5");
    ASSERT_NO_THROW(brd.play(black_correct_take));
}

TEST(Chess, Rook) {
    Board brd;
    brd.play(Parser::Parse("H2 H4"));
    brd.play(Parser::Parse("A7 A5"));

    auto white_jump_figure = Parser::Parse("H1 H6");
    ASSERT_ANY_THROW(brd.play(white_jump_figure));

    auto white_correct_vertical_move = Parser::Parse("H1 H3");
    ASSERT_NO_THROW(brd.play(white_correct_vertical_move));

    auto black_take_ally = Parser::Parse("A8 A5");
    ASSERT_ANY_THROW(brd.play(black_take_ally));

    auto black_correct_vertical_move = Parser::Parse("A8 A6");
    ASSERT_NO_THROW(brd.play(black_correct_vertical_move));

    auto white_correct_horizontal_move = Parser::Parse("H3 A3");
    ASSERT_NO_THROW(brd.play(white_correct_horizontal_move));

    auto black_correct_horizontal_move = Parser::Parse("A6 E6");
    ASSERT_NO_THROW(brd.play(black_correct_horizontal_move));

    auto white_uncorrect_move = Parser::Parse("A3 E4");
    ASSERT_ANY_THROW(brd.play(white_uncorrect_move));

    auto white_uncorrect_take = Parser::Parse("A3 E7");
    ASSERT_ANY_THROW(brd.play(white_uncorrect_take));

    auto white_correct_take = Parser::Parse("A3 A5");
    ASSERT_NO_THROW(brd.play(white_correct_take));
}

TEST(Chess, Knight) {
    Board brd;

    auto white_take_alias = Parser::Parse("B1 D2");
    ASSERT_ANY_THROW(brd.play(white_take_alias));

    auto white_correct_move = Parser::Parse("B1 C3");
    ASSERT_NO_THROW(brd.play(white_correct_move));

    auto black_uncorect_move = Parser::Parse("G8 G6");
    ASSERT_ANY_THROW(brd.play(black_uncorect_move));

    auto black_correct_move = Parser::Parse("G8 H6");
    ASSERT_NO_THROW(brd.play(black_correct_move));

    auto white_uncorect_move = Parser::Parse("C3 D4");
    ASSERT_ANY_THROW(brd.play(white_uncorect_move));
}

TEST(Chess, Bishop) {
    Board brd;

    auto white_jamp = Parser::Parse("F1 H3");
    ASSERT_ANY_THROW(brd.play(white_jamp));

    brd.play(Parser::Parse("G2 G3"));
    brd.play(Parser::Parse("G7 G6"));

    auto white_corect_move = Parser::Parse("F1 H3");
    ASSERT_NO_THROW(brd.play(white_corect_move));

    auto black_uncorect_move = Parser::Parse("F8 G6");
    ASSERT_ANY_THROW(brd.play(black_uncorect_move));

    auto black_corect_move = Parser::Parse("F8 H6");
    ASSERT_NO_THROW(brd.play(black_corect_move));

    auto white_uncorect_take = Parser::Parse("H3 C8");
    ASSERT_ANY_THROW(brd.play(white_uncorect_take));

    auto white_correct_take = Parser::Parse("H3 D7");
    ASSERT_NO_THROW(brd.play(white_correct_take));
}

TEST(Chess, Queen) {
    Board brd;

    brd.play(Parser::Parse("E2 E4"));
    brd.play(Parser::Parse("D7 D5"));

    auto white_correct = Parser::Parse("D1 H5");
    ASSERT_NO_THROW(brd.play(white_correct));

    auto black_uncorrect = Parser::Parse("D8 E6");
    ASSERT_ANY_THROW(brd.play(black_uncorrect));

    auto black_correct = Parser::Parse("D8 D6");
    ASSERT_NO_THROW(brd.play(black_correct));

    auto white_uncorrect = Parser::Parse("H5 H8");
    ASSERT_ANY_THROW(brd.play(white_uncorrect));
}

TEST(Chess, King) {
    Board brd;

    brd.play(Parser::Parse("E2 E4"));
    brd.play(Parser::Parse("E7 E6"));

    auto white_uncorrect = Parser::Parse("E1 E3");
    ASSERT_ANY_THROW(brd.play(white_uncorrect));

    auto white_correct = Parser::Parse("E1 E2");
    ASSERT_NO_THROW(brd.play(white_correct));

    auto black_uncorrect = Parser::Parse("E8 E4");
    ASSERT_ANY_THROW(brd.play(black_uncorrect));

    auto black_correct = Parser::Parse("E8 E7");
    ASSERT_NO_THROW(brd.play(black_correct));

    auto white_correct2 = Parser::Parse("E2 D3");
    ASSERT_NO_THROW(brd.play(white_correct2));
}

TEST(Chess, castling) {
    Board brd;

    brd.play(Parser::Parse("B1 A3"));
    brd.play(Parser::Parse("G8 H6"));
    brd.play(Parser::Parse("D2 D4"));
    brd.play(Parser::Parse("E7 E5"));
    brd.play(Parser::Parse("C1 H6"));
    brd.play(Parser::Parse("F8 A3"));
    brd.play(Parser::Parse("D1 D3"));

    auto black_castling = Parser::Parse("E8 H8");
    ASSERT_NO_THROW(brd.play(black_castling));

    brd.play(Parser::Parse("B2 B3"));
    brd.play(Parser::Parse("A3 B2"));
    auto black_blocked_castling = Parser::Parse("E1 A1");
    ASSERT_ANY_THROW(brd.play(black_blocked_castling));


    brd.play(Parser::Parse("H2 H3"));
    brd.play(Parser::Parse("B2 D4"));
    ASSERT_NO_THROW(brd.play(black_blocked_castling));
}

TEST(Chess, check) {
    Board brd;
    Piece *king;

    brd.play(Parser::Parse("C2 C4"));
    brd.play(Parser::Parse("D7 D5"));
    brd.play(Parser::Parse("D1 A4"));

    for (auto i: brd.getVectorChess()) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] == 'B' && j->getName()[1] == 'K') {
                king = j;
            }
        }
    }

    ASSERT_TRUE(brd.checkField(king->getY(), king->getX()));
    ASSERT_FALSE(brd.checkMate(*king));
}

TEST(Chess, checkmate) {
    Board brd;
    Piece *king;

    brd.play(Parser::Parse("G2 G4"));
    brd.play(Parser::Parse("E7 E5"));
    brd.play(Parser::Parse("F2 F3"));
    brd.play(Parser::Parse("D8 H4"));

    for (auto i: brd.getVectorChess()) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] == 'W' && j->getName()[1] == 'K') {
                king = j;
            }
        }
    }

    ASSERT_TRUE(brd.checkField(king->getY(), king->getX()));
    ASSERT_TRUE(brd.checkMate(*king));
}

const std::vector<std::string> turn = {
        "E2 E4",
        "E7 E5",
        "G1 F3",
        "D8 F6",
        "F1 C4",
        "F8 C5",
        "D2 D3",
        "B8 C6",
        "B1 C3",
        "G8 E7",
        "C1 G5",
        "F6 G6",
        "G5 E7",
        "G6 G2",
        "H1 G1",
        "G2 F2"
};

TEST(Chess, Chess_game) {
    Board brd;

    for (auto i : turn) {
        brd.play(Parser::Parse(i));
    }

    Piece *king;
    for (auto i: brd.getVectorChess()) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] == 'W' && j->getName()[1] == 'K') {
                king = j;
            }
        }
    }

    ASSERT_TRUE(brd.checkField(king->getY(), king->getX()));
    ASSERT_TRUE(brd.checkMate(*king));
}

TEST(Chess, Chess_game_with_load){
    Board* brd = new Board;

    for (size_t i = 0; i < turn.size()/2; ++i) {
        brd->play(Parser::Parse(turn.at(i)));
    }

    brd->save("test_save.txt");
    delete brd;
    brd = new Board;
    brd->load("test_save.txt");

    for (size_t i = turn.size()/2; i < turn.size(); ++i) {
        brd->play(Parser::Parse(turn.at(i)));
    }

    Piece *king;
    for (auto i: brd->getVectorChess()) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] == 'W' && j->getName()[1] == 'K') {
                king = j;
            }
        }
    }

    ASSERT_TRUE(brd->checkField(king->getY(), king->getX()));
    ASSERT_TRUE(brd->checkMate(*king));
}

int main() {
    ::testing::InitGoogleTest();
    return RUN_ALL_TESTS();
}