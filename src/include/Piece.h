#ifndef CHESS_PIECE_H
#define CHESS_PIECE_H

#include <string>
#include <vector>

class Piece {
protected:
    bool not_move = true;
    std::string name;
    size_t pos_x, pos_y;

    virtual bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) = 0;

public:
    Piece() = default;

    Piece(size_t y, size_t x, const std::string &nm) : name(nm), pos_x(x), pos_y(y) {};

    virtual bool move_in(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board);

    virtual void setPosition(size_t y, size_t x);

    void Moved();

    std::string getName() const;

    size_t getX() const;

    size_t getY() const;

    bool is_notMoved() const;

    virtual ~Piece() {};
};

class Pawn : public Piece {
    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:

    Pawn(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "P") {};
};

class Rook : virtual public Piece {
protected:
    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:

    Rook() = default;

    Rook(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "R") {};
};

class Knight : public Piece {
    const std::vector<int> moves = {-1, -2, -2, -1, -2, 1, -1, 2, 1, 2, 2, 1, 2, -1, 1, -2};

    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:
    Knight(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "N") {};
};

class Bishop : virtual public Piece {
protected:
    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:

    Bishop() = default;

    Bishop(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "B") {};
};

class Queen : public Rook, public Bishop {
protected:
    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:
    Queen() = default;

    Queen(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "Q") {};
};

class King : public Queen {
    bool move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) override;

public:
    King(size_t y, size_t x, const std::string &nm) : Piece(y, x, nm + "K") {};
};

#endif //CHESS_PIECE_H
