#include "../include/Board.h"

Board::Board() {
    chess_board = new std::vector<std::vector<Piece *>>(8, std::vector<Piece *>(8, nullptr));

    std::string tmp = "B";
    size_t pawn = 1, other = 0;
    for (size_t j = 0; j < 2; ++j) {
        for (size_t i = 0; i < chess_board->size(); ++i) {
            chess_board->at(pawn).at(i) = new Pawn(pawn, i, tmp);
            if (i == 0 || i == 7) {
                chess_board->at(other).at(i) = new Rook(other, i, tmp);
            } else if (i == 1 || i == 6) {
                chess_board->at(other).at(i) = new Knight(other, i, tmp);
            } else if (i == 2 || i == 5) {
                chess_board->at(other).at(i) = new Bishop(other, i, tmp);
            } else if (i == 3) {
                chess_board->at(other).at(i) = new Queen(other, i, tmp);
            } else {
                chess_board->at(other).at(i) = new King(other, i, tmp);
            }
        }
        tmp = "W";
        pawn = 6;
        other = 7;
    }
    check_chess_board = new std::vector<std::vector<Piece *>>(*chess_board);
}

void Board::printBoard() {

    std::cout << std::setw(16) << "WHITE" << std::endl;
    std::cout << "   A";
    for (char i = 'B'; i < 'I'; ++i) {
        std::cout << "  " << i;
    }
    std::cout << std::endl;

    int in = 8;//
    for (auto i : *chess_board) {
        std::cout << in;
        for (auto j : i) {
            if (j != nullptr) {
                std::cout << '|' << j->getName();
            } else {
                std::cout << "|__";
            }
        }
        std::cout << '|' << in << std::endl;
        --in;
    }

    std::cout << "   A";
    for (char i = 'B'; i < 'I'; ++i) {
        std::cout << "  " << i;
    }
    std::cout << std::endl;
    std::cout << std::setw(16) << "BLACK" << std::endl;
}

void Board::play(std::pair<std::pair<size_t, size_t>, std::pair<size_t, size_t>> val) {

    Piece *moved = check_chess_board->at(val.first.first).at(val.first.second);
    Piece *move_to = check_chess_board->at(val.second.first).at(val.second.second);

    if (moved == nullptr) {
        throw std::invalid_argument("empty field");
    }

    if (moved->getName()[0] == player) {
        if (move_to != nullptr) {
            if (move_to->getName()[0] == moved->getName()[0]) {
                if (move_to->getName()[1] == 'R' &&
                    moved->getName()[1] == 'K') {
                    if (!castling(move_to->getX(), moved->getY(), moved->getX())) {
                        throw std::invalid_argument("Castling impossible");
                    }
                } else {
                    throw std::invalid_argument("field is occupied by your figure");
                }
            } else {
                if (!moved->move_in(move_to->getY(), move_to->getX(), *check_chess_board)) {
                    throw std::invalid_argument("figure can't move in this field");
                } else {
                    delete move_to;
                    check_chess_board->at(val.second.first).at(val.second.second) = moved;
                    check_chess_board->at(val.first.first).at(val.first.second) = nullptr;
                }
            }
        } else {
            if (!moved->move_in(val.second.first, val.second.second, *check_chess_board)) {
                throw std::invalid_argument("figure can't move in this field");
            } else {
                check_chess_board->at(val.second.first).at(val.second.second) = moved;
                check_chess_board->at(val.first.first).at(val.first.second) = nullptr;
            }
        }
    } else {
        throw std::invalid_argument("Not you figure");
    }

    updateBoard(*check_chess_board);

    for (auto i: *check_chess_board) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] == player && j->getName()[1] == 'K') {
                if (checkField(j->getY(), j->getX())) {
                    if (checkMate(*j)) {
                        throw std::invalid_argument("checkmate!");
                    } else {
                        throw std::invalid_argument("You have check!");
                    }
                }
            }
        }
    }

    if (player == 'W') {
        player = 'B';
    } else {
        player = 'W';
    }
    moved->Moved();
    *chess_board = *check_chess_board;
}

Board::~Board() {

    for (auto i : *chess_board) {
        for (auto j : i) {
            delete j;
        }
    }
    delete chess_board;
    delete check_chess_board;
}

bool Board::checkField(size_t y, size_t x) {
    for (auto i: *check_chess_board) {
        for (auto j:i) {
            if (j != nullptr && j->getName()[0] != player) {
                if (j->move_in(y, x, *check_chess_board)) {
                    return true;
                }
            }
        }
    }
    return false;
}

bool Board::castling(size_t xR, size_t y, size_t xK) {
    int bias_rook;
    int bias_king;

    if (check_chess_board->at(y).at(xK)->is_notMoved() && check_chess_board->at(y).at(xR)->is_notMoved()) {
        if (xK > xR) {
            for (size_t i = 2; i <= xK; ++i) {
                if (check_chess_board->at(y).at(i) == nullptr || i == xK) {
                    if (checkField(y, i)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            bias_rook = 3;
            bias_king = -2;
        } else {
            for (size_t i = 6; i >= xK; --i) {
                if (check_chess_board->at(y).at(i) == nullptr || i == xK) {
                    if (checkField(y, i)) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            bias_king = 2;
            bias_rook = -2;
        }
    } else {
        return false;
    }
    check_chess_board->at(y).at(static_cast<size_t >(static_cast<int>(xK) + bias_king)) = check_chess_board->at(y).at(
            xK);
    check_chess_board->at(y).at(xK) = nullptr;
    check_chess_board->at(y).at(static_cast<size_t >(static_cast<int>(xR) + bias_rook)) = check_chess_board->at(y).at(
            xR);
    check_chess_board->at(y).at(xR) = nullptr;
    updateBoard(*check_chess_board);
    return true;
}

bool Board::checkMate(Piece &figure) {

    for (size_t i = 0; i < check_chess_board->size(); ++i) {
        for (size_t j = 0; j < check_chess_board->size(); ++j) {
            for (size_t ii = 0; ii < check_chess_board->size(); ++ii) {
                for (size_t jj = 0; jj < check_chess_board->size(); ++jj) {

                    if (check_chess_board->at(i).at(j) != nullptr &&
                        check_chess_board->at(i).at(j)->getName()[0] == player) {

                        if (check_chess_board->at(i).at(j)->getName()[1] != 'K' &&
                            check_chess_board->at(i).at(j)->move_in(ii, jj, *check_chess_board)) {

                            check_chess_board->at(ii).at(jj) = check_chess_board->at(i).at(j);
                            check_chess_board->at(i).at(j) = nullptr;
                            updateBoard(*check_chess_board);

                            if (!checkField(figure.getY(), figure.getX())) {
                                return false;
                            }
                            *check_chess_board = *chess_board;
                            updateBoard(*check_chess_board);
                        }
                    }
                }
            }
        }
    }
    checkmate = true;
    return checkmate;
}

void Board::updateBoard(std::vector<std::vector<Piece *>> &board) {

    for (size_t i = 0; i < board.size(); ++i) {
        for (size_t j = 0; j < board.size(); ++j) {
            if (board.at(i).at(j) != nullptr) {
                board.at(i).at(j)->setPosition(i, j);
            }
        }
    }
}

void Board::save(const char *path) {

    std::ofstream out;
    out.open(path);



    out << player << std::endl;

    for (auto i : *chess_board) {
        for (auto j : i) {
            if (j != nullptr) {
                out << j->getName() << j->getY() << j->getX() << std::endl;
            }
        }
    }

    out.close();
}

void Board::load(const char *path) {
    std::ifstream in(path);

    if(!in.is_open()){
        throw std::invalid_argument("save file not found");
    }

    std::string str, pl;
    size_t y, x;
    bool first = true;

    this->~Board();

    chess_board = new std::vector<std::vector<Piece *>>(8, std::vector<Piece *>(8, nullptr));

    while (std::getline(in, str)) {
        if (first) {
            if(str != "W" && str != "B"){
                throw std::invalid_argument("uncorect save file");
            }
            player = str[0];
            first = false;
            continue;
        }

        y = static_cast<size_t>(str[2] - '0');
        x = static_cast<size_t>(str[3] - '0');
        pl = std::string(std::string(str.begin(), str.begin() + 1));

        switch (str[1]) {
            case 'R': {
                chess_board->at(y).at(x) = new Rook(y, x, pl);
                break;
            }
            case 'N': {
                chess_board->at(y).at(x) = new Knight(y, x, pl);
                break;
            }
            case 'B': {
                chess_board->at(y).at(x) = new Bishop(y, x, pl);
                break;
            }
            case 'K': {
                chess_board->at(y).at(x) = new King(y, x, pl);
                break;
            }
            case 'Q': {
                chess_board->at(y).at(x) = new Queen(y, x, pl);
                break;
            }
            case 'P':{
                chess_board->at(y).at(x) = new Pawn(y, x, pl);
                break;
            }
            default: {
                throw std::invalid_argument("uncorect save file");
            }
        }
    }

    check_chess_board = new std::vector<std::vector<Piece *>>(*chess_board);
}

bool Board::is_checkMate() const {
    return checkmate;
}

std::vector<std::vector<Piece *>> &Board::getVectorChess() const {
    return *chess_board;
}

char Board::currentPlayer() const {
    return player;
}
