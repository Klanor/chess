#include "include/Piece.h"

bool Pawn::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {

    if (board.at(y).at(x) != nullptr) {
        if (abs(static_cast<int>(x) - static_cast<int>(pos_x)) == 1) {
            if (name[0] == 'B') {
                if (pos_y + 1 == y) {
                    return true;
                }
            } else {
                if (pos_y - 1 == y) {
                    return true;
                }
            }
        }
        return false;
    } else {
        if (pos_x == x) {
            if (name[0] == 'B') {
                if (y - pos_y <= 2) {
                    if (pos_y == 1 || y - pos_y == 1) {
                        return true;
                    }
                }
            } else {
                if (pos_y - y <= 2) {
                    if (pos_y == 6 || pos_y - y == 1) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool Rook::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {

    if (pos_y == y) {
        if (pos_x > x) {
            for (size_t i = pos_x - 1; i > x; --i) {
                if (board.at(pos_y).at(i) != nullptr) {
                    return false;
                }
            }
        } else {
            for (size_t i = pos_x + 1; i < x; ++i) {
                if (board.at(pos_y).at(i) != nullptr) {
                    return false;
                }
            }
        }
        return true;
    } else if (pos_x == x) {
        if (pos_y > y) {
            for (size_t i = pos_y - 1; i > y; --i) {
                if (board.at(i).at(pos_x) != nullptr) {
                    return false;
                }
            }
        } else {
            for (size_t i = pos_y + 1; i < y; ++i) {
                if (board.at(i).at(pos_x) != nullptr) {
                    return false;
                }
            }
        }
        return true;
    }
    return false;
}

bool Knight::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {
    for (size_t i = 0; i != moves.size(); i += 2) {
        if ((static_cast<int>(pos_x) + moves.at(i + 1)) == x && (static_cast<int>(pos_y) + moves.at(i)) == y) {
            return true;
        }
    }
    return false;
}

bool Bishop::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {

    if (pos_x > x && pos_y > y) {
        for (size_t i = pos_y - 1, j = pos_x - 1; i > y || j > x; --i, --j) {
            if (x == j || i == y) {
                return false;
            }
            if (board.at(i).at(j) != nullptr) {
                return false;
            }
        }
    } else if (pos_x < x && pos_y > y) {
        for (size_t i = pos_y - 1, j = pos_x + 1; i > y || j < x; --i, ++j) {
            if (x == j || i == y) {
                return false;
            }
            if (board.at(i).at(j) != nullptr) {
                return false;
            }
        }
    } else if (pos_x > x && pos_y < y) {
        for (size_t i = pos_y + 1, j = pos_x - 1; i < y || j > x; ++i, --j) {
            if (x == j || i == y) {
                return false;
            }
            if (board.at(i).at(j) != nullptr) {
                return false;
            }
        }
    } else if (pos_x < x && pos_y < y) {
        for (size_t i = pos_y + 1, j = pos_x + 1; i < y || j < x; ++i, ++j) {
            if (x == j || i == y) {
                return false;
            }
            if (board.at(i).at(j) != nullptr) {
                return false;
            }
        }
    } else {
        return false;
    }
    return true;
}

bool Queen::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {
    if (x == pos_x || y == pos_y) {
        return Rook::move(y, x, board);
    }
    return Bishop::move(y, x, board);

}

bool King::move(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {

    if (abs(static_cast<int>(x) - static_cast<int>(pos_x)) == 1 ||
        abs(static_cast<int>(y) - static_cast<int>(pos_y)) == 1) {
        return Queen::move(y, x, board);
    }
    return false;
}

bool Piece::move_in(size_t y, size_t x, const std::vector<std::vector<Piece *>> &board) {
    if (pos_y != y || pos_x != x) {
        return move(y, x, board);
    }
    return false;
}

void Piece::setPosition(size_t y, size_t x) {
    pos_x = x;
    pos_y = y;
}

std::string Piece::getName() const {
    return name;
}

size_t Piece::getX() const {
    return pos_x;
}

size_t Piece::getY() const {
    return pos_y;
}

void Piece::Moved() {
    not_move = false;
}

bool Piece::is_notMoved() const {
    return not_move;
}