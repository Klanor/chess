#include "../include/Board.h"
#include "../include/Parser.h"

int main() {
    Board *board = new Board;
    const char *file = "save.txt";
    std::exception *exception = nullptr;

    std::string str;

    while (!board->is_checkMate()) {
        board->printBoard();
        if (exception != nullptr) {
            std::cout << exception->what() << std::endl;
            exception = nullptr;
        }
        std::cout << (board->currentPlayer() == 'W' ? "WHITE(W)" : "BLACK(B)")
                  << " turn (move: \"A1 A3\", other command: \"EXIT\", \"SAVE\", \"LOAD\", \"RESTART\")" << std::endl;

        try {
            std::getline(std::cin, str);
            if (str == "EXIT") {
                break;
            } else if (str == "SAVE") {
                board->save(file);
                std::cout << "Game saved!" << std::endl;
            } else if (str == "LOAD") {
                board->load(file);
                std::cout << "Game load successful!" << std::endl;
            } else if (str == "RESTART") {
                delete board;
                board = new Board;
            } else {
                board->play(Parser::Parse(str));
            }
        } catch (std::exception &ex) {
            exception = &ex;
        }
    }

    delete board;
    return 0;
}
