#include <stdexcept>
#include "../include/Parser.h"

const std::vector<int> Parser::cof = {6, 4, 2, 0, -2, -4, -6, -8};

std::pair<std::pair<size_t, size_t>, std::pair<size_t, size_t>>
Parser::Parse(const std::string &str) {
    std::pair<std::pair<size_t, size_t>, std::pair<size_t, size_t >> res;

    if (str.size() == 5) {
        std::string p1(str.begin(), str.begin() + str.find_first_of(' '));
        std::string p2(str.begin() + str.find_first_of(' ') + 1, str.end());

        try {
            res.first.first = parseY(&p1[1]);
            res.first.second = parseX(p1[0]);
            res.second.first = parseY(&p2[1]);
            res.second.second = parseX(p2[0]);
        } catch (...) {
            throw;
        }
    } else {
        throw std::invalid_argument("Uncorrect input, try again");
    }

    return res;
}

size_t Parser::parseY(const char *val) {
    int res = atoi(val);

    if (res >= 1 && res <= 8) {
        return static_cast<size_t>(res + cof.at(res - 1));
    } else {
        throw std::invalid_argument("Uncorrect input, try again");
    }
}

size_t Parser::parseX(char val) {

    if (val >= 'A' && val <= 'H') {
        return static_cast<size_t>(val - 'A');
    } else {
        throw std::invalid_argument("Uncorrect input, try again");
    }
}
